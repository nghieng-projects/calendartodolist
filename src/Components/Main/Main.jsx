import React, {useEffect, useState} from 'react'
import './Main.scss';
import List from "../List";
import InputPlace from "../InputPlace";
import {toast, ToastContainer} from "react-toastify";
import Calendar from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import 'react-toastify/dist/ReactToastify.css';
import {addTodoApi, updateTodoApi, deleteTodoApi, getTodoApi} from '../../Fetchers/todo.fetcher'

const Main = () => {
    const [list, setList] = useState([])
    const [itemList, setItemList] = useState({
        todo: "",
        time: "",
        active: false
    })

    const [sum, setSum] = useState(0)
    const [finished, setFinished] = useState(0)
    const [date, setDate] = useState(new Date())

    const handleChange = ({target}) => {
        const value = target.value;
        //console.log({value})
        setItemList((PrevState) => {
            return {...PrevState, todo: value}
        })
    }

    const handle = (event) => {
        if (event.keyCode === 13) {
            handleAdd()
        }
    }
    const toggleCheck = (index) => {
        console.log({index})
        const chooseItem = list[index]
        chooseItem.active = !chooseItem.active
        list[index] = chooseItem
        setList([...list])
        console.log(list[index])
        handleUpdate(list[index])
    }


    const toastlifyDelete = () => {
        return toast.success('Delete successfully!', {
            position: "top-right",
            className: "toastlify",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined
        })
    }

    const handleAdd = () => {
        if (itemList.todo !== '') {
            const dateString = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
            itemList.time = dateString
            console.log(itemList)
            addTodoApi(itemList).then((data) => {
                console.log({data})
                const currentList = list
                currentList.push(data)
                setList([...currentList])
                setItemList({todo: "",time:"", active: false})
                getCurrentData(date)
            })
        }
    }

    const handleUpdate = (todoItem) => {
        updateTodoApi(todoItem).then(() => {
            getCurrentData(date);
        })
    }

    const handleDelete = (id) => {
        const alert = window.confirm('do you want to delete ?')
        if (alert) {
            deleteTodoApi(id).then(() => {
                toastlifyDelete()
                getCurrentData(date)
            })
        }
    }

    const getCurrentData = (currentDate) => {
        const time = `${currentDate.getFullYear()}-${currentDate.getMonth()+1}-${currentDate.getDate()}`
        getTodoApi(time).then((data) => {
            console.log({data}) //Array.isArray(data)
            if(Array.isArray(data)){
                setList(data)
                //set summary information
                setSum(data.length)
                const checked = data.filter((item)=>{return item.active === true})
                setFinished(checked.length)
            }
        })
    }

    //select calendar and show date

    const onChangeDate = (date) => {
        console.log(date)
        setDate(date)
        getCurrentData(date);
        toggleShowDate();

    }

    //display calendar table when toggle click
    const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const [showDate, setShowDate] = useState(false)
    const toggleShowDate = () => {
        setShowDate(!showDate)
    }

    useEffect(() => {
        getCurrentData(date);
    }, [])

    return (
        <div className="main">
            <div className="boxTodoList">
                <div className="border">
                    <div className="header">
                        <div className="calendar">
                            <div className="textCalendar" onClick={toggleShowDate} >
                                <h3>{weekday[date.getDay()]}, {date.getDate()} </h3>
                                <h4>{month[date.getMonth()]}</h4>
                            </div>
                            <div className="tableCalendar">
                                {
                                    showDate ? <Calendar value={date} onChange={onChangeDate}/> : ""
                                }
                            </div>
                        </div>

                        <div className="sumaryInformation">
                            <div className="itemSumary">
                                <span>{sum}</span>
                                <h4>Total</h4>
                            </div>
                            <div className="itemSumary">
                                <span>{sum - finished}</span>
                                <h4>Remaining</h4>
                            </div>
                            <div className="itemSumary">
                                <span>{finished}</span>
                                <h4>Done</h4>
                            </div>
                        </div>
                    </div>
                    <div className="body">
                        <InputPlace onChange={handleChange} inputValue={itemList} onAdd={handleAdd}
                                    onKeyUp={handle}/>
                        <List list={list}
                              toggle={(index) => {
                                  toggleCheck(index)
                              }}
                              onDelete={(id) => {
                                  handleDelete(id)
                              }}/>
                        <ToastContainer
                            /* position="top-right"
                             autoClose={1000}
                             hideProgressBar={false}
                             newestOnTop={false}
                             closeOnClick
                             rtl={false}
                             pauseOnFocusLoss
                             draggable
                             pauseOnHover*/
                        />
                        <ToastContainer/>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Main