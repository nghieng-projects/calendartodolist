import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faCircleMinus} from "@fortawesome/free-solid-svg-icons";
import './List.scss';

const List = ({list, toggle,onDelete}) => {
    return (
        <div className="listPlace">
            <ul className="list">
                {
                    list.map((item,index) => {
                        return(
                            <li className="item" key={index} style={item.active===true ? {textDecoration:"line-through"} : {textDecoration:"none"}}>
                                <div className="textAndRadio">
                                    <input type="checkbox" onChange={()=>{toggle(index)}} checked={item.active===true ? "checked" : ""}/>
                                    <span className="valueTodo">{item.todo}</span>
                                </div>
                                <FontAwesomeIcon icon={faCircleMinus} className="icondelete" onClick={()=>{onDelete(item.id)}}/>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}
export default List