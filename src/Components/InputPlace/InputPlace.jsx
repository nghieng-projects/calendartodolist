import React from "react";
import './InputPlace.scss'

const InputPlace = ({onChange, inputValue, onAdd, onKeyUp}) => {
    return (
        <div className="inputPlace">
            <input type="text" placeholder="Enter something..." onChange={onChange}
                   onKeyUp={onKeyUp} value={inputValue.todo}/>
            <button onClick={onAdd}>+</button>
        </div>
    )
}
export default InputPlace