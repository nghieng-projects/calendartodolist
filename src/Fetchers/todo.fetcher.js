const todoApi = process.env.REACT_APP_TODO_API;

export const addTodoApi = (itemList) => {
    return fetch(`${todoApi}/todos`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(itemList)
    }).then((response) => {
        return response.json()
    });
}

export const updateTodoApi = (todoItem) => {
    return fetch(`${todoApi}/todos/${todoItem.id}`, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(todoItem)
    }).then((response) => {
        return response.json()
    })
}

export const deleteTodoApi = (id) => {
    console.log(id)
    return fetch(`${todoApi}/todos/${id}`, {
        method: 'DELETE'
    }).then((response) => {
        return response.json()
    })
}

export const getTodoApi = (time) => {
    console.log({time})
    return fetch(`${todoApi}/todos?q=${time}`,{
        headers : {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
        .then((response) => {
            return response.json()
        })
}
